import json
from uuid import uuid4

from packages.config import get_execution_config
from packages.logger import get_logger
from packages.report_client import ReportClient
from packages.storage_client import StorageClient
from packages.strategy_client import StrategyClient

logger = get_logger(__name__)


def run():
    config = get_execution_config()

    report_client = ReportClient(config=config)
    report_id = report_client.create_report(message={"title": f"platform test {uuid4()}", "convert": None})["id"]
    report_data, tested, reported = [], 0, 0

    storage_client = StorageClient(config=config)
    messages = storage_client.messages(request={"name": "strategy_service_mock", "limit": 1000, "offset": 1000})

    strategy_client = StrategyClient(config=config)

    for item in messages["data"]:
        mef_response = strategy_client.strategy_service_mock(message=item["request"])
        tested += 1
        report_data.append({"id": item["app_id"], "first": item["response"], "second": mef_response})

        if len(report_data) == 100:
            report_client.upload_report({"report_id": report_id, "data": report_data})
            reported += 100
            report_data = []

        logger.info(f"tested={tested}, reported={reported}")

    if len(report_data) > 0:
        report_client.upload_report({"report_id": report_id, "data": report_data})
        reported += len(report_data)
        report_data = []
        logger.info(f"tested={tested}, reported={reported}")


if __name__ == "__main__":
    run()
