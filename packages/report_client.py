import logging

from packages.config import ExecutionConfig
from packages.http_client import HttpClient

logger = logging.getLogger(__name__)


class ReportClient:
    def __init__(self, config: ExecutionConfig):
        self._http_client = HttpClient(base_url=config.REPORT_ADDRESS)

    def create_report(self, message: dict) -> dict:
        logger.debug("try to create new report")
        response = self._http_client.perform_post(path="report", request=message)
        return response

    def upload_report(self, message: dict) -> dict:
        logger.debug("try to upload report data")
        response = self._http_client.perform_post(path="upload", request=message)
        return response
