import functools
from typing import Optional
from urllib.parse import urljoin

import requests
import urllib3
from requests import Response
from requests.adapters import HTTPAdapter

from packages.logger import get_logger, RECORD_REQUEST, RECORD_RESPONSE

logger = get_logger(__name__)


class LoggedHTTPAdapter(HTTPAdapter):
    def __init__(self, **kwargs):
        super().__init__()
        self.kwargs = kwargs

    @functools.wraps(HTTPAdapter.send)
    def send(self, request, *args, **kwargs):
        logger.debug("Request:", extra={RECORD_REQUEST: request})
        response = HTTPAdapter.send(self, request, **self.kwargs)
        logger.debug("Response:", extra={RECORD_RESPONSE: response})
        return response


class HttpClient:
    def __init__(self, base_url: str, cert: Optional[str] = None):
        self.base_url = base_url
        self.session = requests.Session()
        self.session.cert = cert
        self.session.verify = False
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        for scheme in ["http://", "https://"]:
            self.session.mount(scheme, LoggedHTTPAdapter(cert=self.session.cert, verify=self.session.verify))

    def _request(
            self,
            method: str,
            uri: str,
            data: Optional[dict] = None,
            json: Optional[dict] = None,
            headers: Optional[dict] = None,
            params: Optional[dict] = None,
    ) -> Response:
        url = urljoin(self.base_url, uri)
        response = self.session.request(method=method, headers=headers, url=url, data=data, json=json, params=params)
        return response

    def perform_get(self, path: str, request: dict) -> dict:
        response = self._request(method="GET", uri=path, params=request)
        assert response.status_code == 200, f"Wrong response.status_code: {response.status_code}"
        return response.json()

    def perform_post(self, path: str, request: dict) -> dict:
        response = self._request(method="POST", uri=path, json=request)
        assert response.status_code in (200, 201), f"Wrong response.status_code: {response.status_code}"
        return response.json()
