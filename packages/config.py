import os
from dataclasses import dataclass
from typing import Optional


@dataclass
class ExecutionConfig:
    LOG_LEVEL: Optional[str]
    STORAGE_ADDRESS: str
    STRATEGY_SERVICE_ADDRESS: str
    STRATEGY_SERVICE_CLIENT_CERT: Optional[str]
    REPORT_ADDRESS: str


class ExecutionConfigEnv(ExecutionConfig):
    def __init__(self):
        self.data = os.environ

    def get(self):
        # self.LOG_LEVEL = "info"
        # self.STORAGE_ADDRESS = "http://localhost:8081/"
        # self.STRATEGY_SERVICE_ADDRESS = "http://localhost:8082/api/"
        # self.STRATEGY_SERVICE_CLIENT_CERT = None
        # self.REPORT_ADDRESS = "http://localhost:8083/api/v1/"
        self.LOG_LEVEL = self.data["LOG_LEVEL"]
        self.STORAGE_ADDRESS = self.data["STORAGE_ADDRESS"]
        self.STRATEGY_SERVICE_ADDRESS = self.data["STRATEGY_SERVICE_ADDRESS"]
        self.STRATEGY_SERVICE_CLIENT_CERT = self.data.get("STRATEGY_SERVICE_CLIENT_CERT")
        self.REPORT_ADDRESS = self.data["REPORT_ADDRESS"]
        return self


def get_execution_config():
    config = ExecutionConfigEnv()
    return config.get()
