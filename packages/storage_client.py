import logging

from packages.config import ExecutionConfig
from packages.http_client import HttpClient

logger = logging.getLogger(__name__)


class StorageClient:
    def __init__(self, config: ExecutionConfig):
        self._http_client = HttpClient(base_url=config.STORAGE_ADDRESS)

    def messages(self, request: dict) -> dict:
        logger.debug("try to get data from message storage")
        response = self._http_client.perform_get(path="messages", request=request)
        return response
