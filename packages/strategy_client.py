import logging

from packages.config import ExecutionConfig
from packages.http_client import HttpClient

logger = logging.getLogger(__name__)


class StrategyClient:
    def __init__(self, config: ExecutionConfig):
        self._http_client = HttpClient(
            base_url=config.STRATEGY_SERVICE_ADDRESS, cert=config.STRATEGY_SERVICE_CLIENT_CERT
        )

    def strategy_service_mock(self, message: dict) -> dict:
        logger.debug("performing request strategy_service_mock")
        response = self._http_client.perform_post(path="strategy_service_mock", request=message)
        return response
