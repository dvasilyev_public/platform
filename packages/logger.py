import functools
import logging
import sys
import textwrap

from packages.config import get_execution_config

RECORD_REQUEST = "record_request"
RECORD_RESPONSE = "record_response"


class _Formatter(logging.Formatter):
    @staticmethod
    def _format_headers(headers):
        return "\n".join(f"{field}: {value}" for field, value in headers.items())

    def _format_request(self, request):
        headers = self._format_headers(request.headers)
        return textwrap.dedent(f"{request.method} {request.url}\n{headers}\n\n{request.body}")

    def _format_response(self, response):
        headers = self._format_headers(response.headers)

        def _trim_content(content):
            content_type = response.headers.get("Content-Type")
            if content_type and "text/html" in content_type:
                return f"{content[:20]} ... // trimmed html content"
            return content

        content = _trim_content(response.content.decode(response.encoding or "utf-8"))
        return textwrap.dedent(f"{response.status_code} {response.reason} {response.url}\n{headers}\n\n{content}")

    @functools.wraps(logging.Formatter.format)
    def format(self, record):
        message = logging.Formatter.format(self, record)
        if hasattr(record, RECORD_REQUEST):
            message += f"\n{self._format_request(getattr(record, RECORD_REQUEST))}"
        elif hasattr(record, RECORD_RESPONSE):
            message += f"\n{self._format_response(getattr(record, RECORD_RESPONSE))}"
        return message


def get_logger(name: str) -> logging.Logger:
    config = get_execution_config()
    level = {
        "info": logging.INFO,
        "debug": logging.DEBUG,
    }[config.LOG_LEVEL]

    formatter = _Formatter("%(asctime)s [%(levelname)s] [%(name)s:%(filename)s:%(lineno)d] %(message)s")

    logger = logging.getLogger(name)
    logger.setLevel(level=level)

    stream_handler = logging.StreamHandler(stream=sys.stdout)
    stream_handler.setFormatter(formatter)
    stream_handler.setLevel(level=level)
    logger.addHandler(stream_handler)

    return logger
