# platform


### message_storage

GET http://localhost:8081/messages?name=strategy_service_mock&limit=10&offset=90
```json
{
  "result": "ok",
  "name": "strategy_service_mock",
  "limit": "10",
  "offset": "90",
  "count": 1000,
  "data": [
    {"app_id":91,"request":{"a":1,"b":[4,3,7]},"response":{"a":1,"b":[4,3,7]}},
    {"app_id":92,"request":{"a":4,"b":[0,6,7]},"response":{"a":4,"b":[0,6,7]}},
    {"app_id":93,"request":{"a":8,"b":[0,6,3]},"response":{"a":8,"b":[0,6,3]}},
    {"app_id":94,"request":{"a":0,"b":[3,1,6]},"response":{"a":0,"b":[3,1,6]}},
    {"app_id":95,"request":{"a":8,"b":[5,4,6]},"response":{"a":8,"b":[5,4,6]}},
    {"app_id":96,"request":{"a":6,"b":[2,6,4]},"response":{"a":6,"b":[2,6,4]}},
    {"app_id":97,"request":{"a":4,"b":[9,5,5]},"response":{"a":4,"b":[9,5,5]}},
    {"app_id":98,"request":{"a":3,"b":[1,6,3]},"response":{"a":3,"b":[1,6,3]}},
    {"app_id":99,"request":{"a":6,"b":[3,5,0]},"response":{"a":6,"b":[3,5,0]}},
    {"app_id":100,"request":{"a":7,"b":[9,7,8]},"response":{"a":7,"b":[9,7,8]}}
  ]
}
```

### strategy_service_mock

POST http://localhost:8082/api/strategy_service_mock
```json
{"a":9,"b":[6,5,7]}
```
```json
{"a":"9","b":["6","5","7"]}
```

### report

POST http://localhost:8083/api/v1/report
```json
{
  "title": "my report",
  "convert": {
    "convert_first": [
      ["int", ".Applicant.APP_No"],
      ["float", ".Result.CP_Loan_Limit_Value[*]"]
    ],
    "convert_second": [
      ["float", ".Result.CP_Loan_Limit_Value[*]"]
    ],
    "ignore": [
      ".Z_Flaf"
    ]
  }
}
```
```json
{
  "id": 12,
  "result": "ok",
  "title": "my report"
}
```

POST http://localhost:8083/api/v1/upload
```json
{
  "report_id": 1,
  "data": [{
      "id": 100,
      "first": {"a": 1, "b":1, "c":1},
      "second": {"a": 2, "b":3, "c":2}
    },{
      "id": 201,
      "first": {"a": 1, "b":1, "c":1},
      "second": {"a": 2, "b":3, "c":2}
    },{
      "id": 211,
      "first": {"a": 1, "b":1, "c":1},
      "second": {"a": 2, "b":4, "c":2}
    }
  ]
}
```
```json
{
    "result": "ok"
}
```


http://20.82.144.28:8083/

http://localhost:8083/
